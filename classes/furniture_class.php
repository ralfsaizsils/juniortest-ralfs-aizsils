<?php
    class Furniture extends Product 
    {
        public function setSpecialInfo() : string
        {
            return "Dimensions: " . $_POST['HEIGHT'] ."x". $_POST['WIDTH'] ."x". $_POST['LENGTH'];
        }
        public function echoForm()
        {
            echo "
            <label for='height'>Height (CM) </label>
            <input type='text' id='height' name='HEIGHT'><br>
            <small id='height-msg'></small><br>
            <label for='width'>Width (CM) </label>
            <input type='text' id='width' name='WIDTH'><br>
            <small id='width-msg'></small><br>
            <label for='length'>Length (CM) </label>
            <input type='text' id='length' name='LENGTH'><br>
            <small id='lenght-msg'></small><br>
            <p>Please, provide dimensions in CM</p>
            ";
        }
    }
?>