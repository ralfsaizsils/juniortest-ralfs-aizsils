<?php
    class Book extends Product 
    {
        public function setSpecialInfo() : string
        {
            return "Weight: " . $_POST['WEIGHT'] . " KG";
        }
        public function echoForm()
        {
            echo "
            <label for='weight'>Weight (KG) </label>
            <input type='text' id='weight' name='WEIGHT'><br>
            <small id='weight-msg'></small><br>
            <p>Please, provide weight in KG</p>
            ";
        }
    }
?>