<?php
    interface ProductInterface
    {
        public function setProduct();
        public function removeProduct();
        public function addInfo(string $name, string $sku, float $price, int $npk, string $info);
        public function setSpecialInfo() : string;
        public function productListing();
        public function compareInfo(string $name, string $sku, float $price, string $info) : bool;
        public function getSKU() : string;
        public function echoForm();
    }

    abstract class Product extends Dbh implements ProductInterface
    {
        private $sku;
        private $name;
        private $price;
        private $info;
        private $npk;

        public function setProduct()
        {
            $sql = "INSERT INTO products (SKU, `name`, price, info) VALUES (?, ?, ?, ?)";
            $stmt = Dbh::connect()->prepare($sql);
            $stmt->execute([$this->sku,$this->name,$this->price,$this->info]);
        }

        public function removeProduct()
        {
            $sql = "DELETE FROM products WHERE products.NPK = ?";
            $stmt = Dbh::connect()->prepare($sql);
            $stmt->execute([$this->npk]);
        }

        public function addInfo(string $name, string $sku, float $price, int $npk, string $info)
        {
            $this->sku = $sku;
            $this->name = $name;
            $this->price = $price;
            $this->info = $info;
            $this->npk = $npk;
        }

        public function productListing()
        {
            $productSqear = "<div class='product'>";

            $productSqear .= "<input type='checkbox' class='delete-checkbox' value=". $this->npk . " autocomplete='off'>";
            $productSqear .= "<p>" . $this->sku . "</p>";
            $productSqear .= "<p>" . $this->name . "</p>";
            $productSqear .= "<p>" . number_format((float)$this->price, 2, '.','') . " $</p>";
            $productSqear .= "<p>Size: " . $this->info . "</p>";

            $productSqear .= "</div>";

            echo $productSqear;
        }

        public function compareInfo(string $name, string $sku, float $price, string $info) : bool
        {
            return ($this->name==$name && $this->sku==$sku && $this->price==$price && $this->info==$info);
        }

        public function getSKU() : string
        {
            return $this->sku;
        }
    }
?>
