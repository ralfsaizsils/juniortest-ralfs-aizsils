<?php
    class DVD extends Product 
    {
        public function setSpecialInfo() : string
        {
            return "Size: " . $_POST['SIZE'] . " MB";
        }
        public function echoForm()
        {
            echo "
            <label for='size'>Size (MB) </label>
            <input type='text' id='size' name='SIZE'><br>
            <small id='size-msg'></small><br>
            <p>Please, provide size in MB</p>
            ";
        }
    }
?>