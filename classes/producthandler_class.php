<?php
    include 'dbh_class.php';
    include 'product_class.php';
    include 'basicproduct_class.php';
    include 'dvd_class.php';
    include 'furniture_class.php';
    include 'book_class.php';

    class ProductHandler extends Dbh {

        static public function getProducts()
        {
            $sql = "SELECT * FROM products";
            $stmt = Dbh::connect()->query($sql);

            $product_list = array();
            while($row = $stmt->fetch())
            {
                $product = new BasicProduct();
                $product->addInfo($row['name'],$row['SKU'],$row['price'],$row['NPK'],$row['info']);
                $product_list[$row['NPK']] = $product;
            }
            return $product_list;
        }
        static public function outputProducts(array $product_list)
        {
            foreach($product_list as $product)
            {
                $product->productListing();
            }
        }

        static public function makeProductList_withDeletion()
        {
            $product_list = ProductHandler::getProducts();
            if(isset($_POST['npk_value_array']) ?? null)
            {
                foreach($_POST['npk_value_array'] as $npk)
                {
                    $product_list[$npk]->removeProduct();
                    unset($product_list[$npk]);
                }
            }

            echo ProductHandler::outputProducts($product_list);
        }
        
        static public function makeProductList()
        {
            $product_list = ProductHandler::getProducts();
            echo ProductHandler::outputProducts($product_list);
        }

        static public function addNewProduct() : int
        {
            $function_name_for_type = $_POST['selectedType'];
            $new_product = ProductHandler::$function_name_for_type();
            $new_product->addInfo($_POST['NAME'],$_POST['SKU'],$_POST['PRICE'],0,$new_product->setSpecialInfo());
            if (ProductHandler::testUniqueProducts($new_product)) 
            {
                $new_product->setProduct();
                return 1;
            }
            else
            {
                return 0;
            }
        }
        static public function prepareProduct(string $function_name_for_type)
        {
            $new_product = ProductHandler::$function_name_for_type();
            return $new_product->echoForm();
        }

        static private function Book() : Book
        {
            return new Book();
        }
        static private function Furniture() : Furniture
        {
            return new Furniture();
        }
        static private function DVD() : DVD
        {
            return new DVD();
        }

        static public function testUniqueProducts(ProductInterface $product) : bool
        {
            $sql = "SELECT `name`, SKU, price, info FROM products  WHERE products.SKU = ? GROUP BY SKU;";
            $stmt = Dbh::connect()->prepare($sql);
            $stmt->execute([$product->getSKU()]);

            $isSKUvalid = true;
            while($row = $stmt->fetch())
            {
                $isSKUvalid = ($product->compareInfo($row['name'],$row['SKU'],$row['price'],$row['info']));
                //print_r([$row['name'],$row['SKU'],$row['price'],$row['info']]);
            }
            return $isSKUvalid;
        }
    }
?>