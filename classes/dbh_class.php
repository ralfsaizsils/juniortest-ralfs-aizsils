<?php
abstract class Dbh {
    //set to local server
    static private $host = "localhost";
    static private $user = "root";
    static private $pwd = "";
    static private $dbName = "product_db";

    // 000webhost
    /*static private $host = "localhost";
    static private $user = "id17753268_scandiweb";
    static private $pwd = "[&bD19J?]oG<EqmW";
    static private $dbName = "id17753268_product_db";*/

    static protected function connect()
    {
        $dsn = 'mysql:host=' . Dbh::$host . ';dbname='. Dbh::$dbName;
        
        $pdo = new PDO($dsn, Dbh::$user, Dbh::$pwd);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }
}
?>