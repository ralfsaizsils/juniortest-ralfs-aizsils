<!DOCTYPE html>
    <html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Product List</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="../sass/style.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script defer src="input_validation.js"></script>
        
        <script type="text/javascript">
            var selected_type;
            function changeProductType(selectedValue)
            {
                selected_type = selectedValue;
                $('#product_info').html('');
                $.ajax({
                    type: 'post',
                    url: '../include/productTypes.php',
                    data: { selectedType : selected_type },
                    success : function(data){
                        $('#product_info').html(data);
                    }
                });
            }
            function cancelBtn()
            {
                window.location.href = "../";
            }
            function saveBtn()
            {
                if (testInputs()) {

                    let post_data = getInputValues();
                    post_data["selectedType"] = selected_type;
                    $.ajax({
                        type: 'post',
                        url: '../include/productSave.php',
                        data: post_data,
                        success : function(data){
                            //console.log(data);
                            if(data==1)
                            {
                                window.location.href = "../";
                            }
                            else
                            {
                                $('#sku-msg').html("This SKU is already in use. Please use a unique SKU or match all the data to the existing product with this SKU.");
                            }
                        }
                    });
                }
            }
        </script>
    </head>
    <body>
        
        <form id="product_form" method = "post">
            <div class="header">
                <h2>Product Add</h2>
                <input type='button' onclick="saveBtn()" name="Save" value="Save" id="Save">
                <input type="button" onclick="cancelBtn()" name="Cancel" value="Cancel">
                <hr>
            </div>
            <div class="prod_form">
                <label for="sku">SKU</label>
                <input type="text" id="sku" name="SKU"><br>
                <small id="sku-msg"></small><br>

                <label for="name">Name</label>
                <input type="text" id="name" name="NAME"><br>
                <small id="name-msg"></small><br>

                <label for="price">Price ($)</label>
                <input type="text" id="price" name="PRICE"><br>
                <small id="price-msg"></small><br>

                <label for="productType">Type Switch</label>
                <select id="productType" onchange="changeProductType(this.value)" autocomplete="off">
                    <option value="" selected></option>
                    <option value="DVD">DVD</option>
                    <option value="Furniture">Furniture</option>
                    <option value="Book">Book</option>
                </select>
                <br>
                <small id="productType-msg"></small><br>

                <div id="product_info"> </div>
            </div>
        </form>
        <div class="footer">
            <hr>
            <p>Scandiweb Test assignment</p>
        </div>
    </body>
 </html> 