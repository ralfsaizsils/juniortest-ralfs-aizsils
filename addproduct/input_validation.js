const _name = document.getElementById('name');
const sku = document.getElementById('sku');
const price = document.getElementById('price');
const productType = document.getElementById('productType');
const product_info = document.getElementById('product_info');
const form = document.getElementById('product_form');
const save_btn = document.getElementById('Save');

function testInputs()
{

    let allSmallMessages = document.getElementsByTagName("small");
    for(let i = 0; i<allSmallMessages.length; i++)
    {
        allSmallMessages.item(i).innerText = "";
    }

    let errors = 0;

    let inputCollection = document.getElementsByTagName("input");
    for(let i = 0; i<inputCollection.length; i++)
    {
        if(inputCollection.item(i).value == "")
        {
            let smallMsg = document.getElementById(inputCollection.item(i).id + "-msg");
            smallMsg.innerText = "Please, submit required data";
            errors+=1;
        }
    }
    if(productType.value == "")
    {
        let smallMsg = document.getElementById(productType.id + "-msg");
        smallMsg.innerText = "Please, submit required data";
        errors+=1;
    }

    if(sku.value.length>20)
    {
        let smallMsg = document.getElementById(sku.id + "-msg");
        smallMsg.innerText = "Please, provide a shorter SKU (less then 20 letters)";
        errors+=1;
    }
    if(_name.value.length>30)
    {
        let smallMsg = document.getElementById(_name.id + "-msg");
        smallMsg.innerText = "Please, provide a shorter name (less then 30 letters)";
        errors+=1;
    }
    if(!number_test(price.value))
    {
        let smallMsg = document.getElementById("price-msg");
        smallMsg.innerText = "Please, provide the data of indicated type";
        errors+=1;
    }
    inputCollection = product_info.getElementsByTagName("input");
    for(let i = 0; i<inputCollection.length; i++)
    {
        if(!number_test(inputCollection.item(i).value))
        {
            let smallMsg = document.getElementById(inputCollection.item(i).id + "-msg");
            smallMsg.innerText = "Please, provide the data of indicated type";
            errors+=1;
        }
        else if(inputCollection.item(i).value.length>15)
        {
            let smallMsg = document.getElementById(inputCollection.item(i).id + "-msg");
            smallMsg.innerText = "Please, provide a smaller number";
            errors+=1;
        }
    }

    return errors<=0;
}


function number_test(value)
{
    return /^\d*\.?\d*$/.test(value);
}

function getInputValues(){
    let values = {};
    let inputCollection = document.getElementsByTagName("input");
    for(let i = 0; i<inputCollection.length; i++)
    {
        values[inputCollection.item(i).name] = inputCollection.item(i).value;
    }
    return values;
}