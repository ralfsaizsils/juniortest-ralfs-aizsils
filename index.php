<!DOCTYPE html>
    <html lang="en" dir="ltr">

    <head>
        <meta charset="utf-8">
        <title>Product List</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="sass/style.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        
        <script type="text/javascript">
            $.ajax({
                type: 'post',
                url: 'include/productList.php',
                data: {},
                success : function(data)
                {
                    $('#list').html(data);
                }
            });
            function deleteAllSelectedProducts() 
            {
                var dltCheckboxes = document.querySelectorAll(".delete-checkbox:checked");
                var npk_value_array = [];
                dltCheckboxes.forEach(element => {
                    npk_value_array.push(element.value);
                });
                $.ajax({
                    type: 'post',
                    url: 'include/productDelete.php',
                    data: { npk_value_array : npk_value_array },
                    success : function(data)
                    {
                        $('#list').html(data);
                    }
                });
            }
            function addProducts() 
            {
                window.location.href = "addproduct/index.php";
            }
        </script>

    </head>

    <body>
        
        <form method ="post" id="product-list">
            <div class = "header">    
                <h2>Product List</h2>
                <input type="button" onclick="addProducts()" name="ADD" value="ADD">
                <input type="button" onclick="deleteAllSelectedProducts()" id="delete-product-btn" name="MASS DELETE" value="MASS DELETE">
                <hr> 
            </div>
            <div class = "grid-container" id="list">
            </div>
            <div class="footer">
                <hr>
                <p>Scandiweb Test assignment</p>
            </div>
        </form>
        
    </body>

 </html> 